package biz.no_ip.palaven.myassistant;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerDialogFragment extends DialogFragment implements
        TimePickerDialog.OnTimeSetListener {

    public static final String ARG_TIME = "time";

    private TimePickedListener mListener;
    private Calendar mTime;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mTime = (Calendar) getArguments().getSerializable(ARG_TIME);

        if (mTime == null) {
            mTime = Calendar.getInstance();
        }

        int hour = mTime.get(Calendar.HOUR_OF_DAY);
        int minute = mTime.get(Calendar.MINUTE);

        // создадим экземпляр класса DatePickerDialog и вернем его
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onAttach(Activity activity) {
        // when the fragment is initially shown (i.e. attached to the activity),
        // cast the activity to the callback interface type
        super.onAttach(activity);
        try {
            mListener = (TimePickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + TimePickedListener.class.getName());
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Пользователь выбрал дату. Выводим его в текстовой метке
        mTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTime.set(Calendar.MINUTE, minute);
        mTime.set(Calendar.SECOND, 0);

        mListener.onTimePicked(mTime);
    }

    public interface TimePickedListener {
        void onTimePicked(Calendar time);
    }
}
