package biz.no_ip.palaven.myassistant.weather;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WeatherPresenter {

    private WeatherModel model;

    public WeatherPresenter() {
        model = new WeatherModel();
    }

    public void loadWeatherDefault(double latitude, double longitude, IWeatherListener listener) {
            model.loadWeather(latitude, longitude, 1, 3)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(listener::putWeather, listener::putError);
    }
}
