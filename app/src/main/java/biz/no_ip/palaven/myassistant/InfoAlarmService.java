package biz.no_ip.palaven.myassistant;

import android.Manifest;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import biz.no_ip.palaven.myassistant.datamodel.CurrentCondition;
import biz.no_ip.palaven.myassistant.datamodel.Hourly;
import biz.no_ip.palaven.myassistant.datamodel.WeatherJson;
import biz.no_ip.palaven.myassistant.weather.IWeatherListener;
import biz.no_ip.palaven.myassistant.weather.WeatherPresenter;

public class InfoAlarmService extends IntentService implements Speech.OnSaidListener{

    MediaPlayer mMediaPlayer;
    Speech mSpeech;
    int currentVolume;
    private final IBinder mBinder = new LocalBinder();
    private OnServiceStopListener mListener;

    public static final int REQUEST_NEXT = 101;
    public static final int REQUEST_STOP = 102;

    public InfoAlarmService() {
        super("infoalarmservice");
    }

    public InfoAlarmService(String name) {
        super(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        showMessage("onBind");
        return mBinder;
    }

    public class LocalBinder extends Binder {
        InfoAlarmService getService() {
            // Return this instance of LocalService so clients can call public methods
            return InfoAlarmService.this;
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        showMessage("onHandleIntent");
        startAlarm();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSpeech = new Speech(this);
        showMessage("onCreate");
    }

    @Override
    public void onDestroy() {
        mSpeech.destroy();
        showMessage("onDestroy");
        super.onDestroy();
    }

    public void setListener(OnServiceStopListener listener){
        mListener = listener;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        showMessage("onStart");
        //startAlarm();
        return super.onStartCommand(intent, flags, startId);
    }

    public void startAlarm() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 8, AudioManager.FLAG_SHOW_UI);
        new PlayIntroTask().execute();
    }

    private void playIntro(){
        mMediaPlayer = MediaPlayer.create(this, R.raw.audiomachine);
        mMediaPlayer.setOnCompletionListener(onStartTrackCompletionListener);
        mMediaPlayer.start();
        showMessage("intro started");
    }

    private void infoMessage(){
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 13, AudioManager.FLAG_SHOW_UI);

        Calendar calendar = Calendar.getInstance();

        mSpeech.sayFirst("Доброе утро.");
        mSpeech.pause();
        mSpeech.say("Сейчас " + Converter.getHoursStr(calendar.get(Calendar.HOUR_OF_DAY))
                + Converter.getMinutesStr(calendar.get(Calendar.MINUTE)));
        mSpeech.pause();
        mSpeech.say(Converter.getDateStr(calendar));
        mSpeech.say(Converter.getWeekDay(calendar.get(Calendar.DAY_OF_WEEK)));
        mSpeech.pause();

        LocationManager locationManager = (LocationManager) getSystemService(
                Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            double lat = 54.81;
            double lon = 56.11;
            if (location != null) {
                lat = location.getLatitude();
                lon = location.getLongitude();
            }
            /*String weatherUrl = "http://api.worldweatheronline.com/free/v2/weather.ashx" +
                    "?key=1abaf4a17e2a72f8f1c17ae13c369" +  //application key
                    "&q="+String.valueOf(lat).substring(0,5)+","+String.valueOf(lon).substring(0, 5)+"" +  //lat-lon Ufa
                    "&num_of_days=1" +                      //only today
                    "&tp=3" +                               //6-hour intervals
                    "&format=json";*/

            ConnectivityManager myConnMgr = (ConnectivityManager) getSystemService(
                    Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkinfo = myConnMgr.getActiveNetworkInfo();

            if (networkinfo != null && networkinfo.isConnected()) {
                //new GetWeatherTask().execute(weatherUrl);
                new WeatherPresenter().loadWeatherDefault(lat, lon, weatherListener);
            } else {
                mSpeech.sayFirst("Нет интернета");
            }
        }else{
            mSpeech.sayFirst("Нет разрешения на геолокацию");
        }

    }

    /*private String getOneWeather(String myurl) throws IOException {
        InputStream inputstream = null;
        String data = "";
        try {
            URL url = new URL(myurl);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setReadTimeout(100000);
            connection.setConnectTimeout(100000);
            connection.setRequestMethod("GET");
            connection.setInstanceFollowRedirects(true);
            connection.setUseCaches(false);
            connection.setDoInput(true);

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) { // 200 OK
                inputstream = connection.getInputStream();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();

                int read = 0;
                while ((read = inputstream.read()) != -1) {
                    bos.write(read);
                }
                byte[] result = bos.toByteArray();
                bos.close();

                data = new String(result);

            } else {
                mSpeech.say("Не удалось получить погоду");
            }
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } finally {
            if (inputstream != null) {
                inputstream.close();
            }
        }
        return data;
    }

    private void parseWeatherJSON(String response) {
        try {
            JSONObject json = new JSONObject(response);
            JSONObject data = json.getJSONObject("data");
            JSONObject current_condition = data.getJSONArray("current_condition").getJSONObject(0);
            int temp = parseInt(current_condition.getString("temp_C"));
            int temp_feels = parseInt(current_condition.getString("FeelsLikeC"));
            float precip = parseFloat(current_condition.getString("precipMM"));

            JSONObject weather = data.getJSONArray("weather").getJSONObject(0);
            JSONArray hourly = weather.getJSONArray("hourly");
            int i = 0;
            int tempD = 100, tempE = 100;
            float precipD = 0.0f, precipE = 0.0f;
            do {
                String time = hourly.getJSONObject(i).getString("time");
                if (time.equals("1300")) {
                    tempD = parseInt(hourly.getJSONObject(i).getString("tempC"));
                    precipD = parseFloat(hourly.getJSONObject(i).getString("precipMM"));
                }
                if (time.equals("1900")) {
                    tempE = parseInt(hourly.getJSONObject(i).getString("tempC"));
                    precipE = parseFloat(hourly.getJSONObject(i).getString("precipMM"));
                }
                i++;
            } while (i < hourly.length());

            mSpeech.say("За окном " + Converter.getDegreesStr(temp) + ", "
                    + Converter.getPrecipitation(precip, temp)
                    + ", температура комфорта " + Converter.getDegreesStr(temp_feels));
            mSpeech.pause();
            mSpeech.say("Днём " + Converter.getDegreesStr(tempD) + ", "
                    + Converter.getPrecipitation(precipD, tempD));
            mSpeech.pause();
            mSpeech.sayLast("Вечером " + Converter.getDegreesStr(tempE) + ", "
                    + Converter.getPrecipitation(precipE, tempE));

        } catch (JSONException e) {
            mSpeech.sayLast("Не удалось получить погоду");
        }
        //stopSelf();
    }

    private int parseInt(String num) {
        try {
            return Integer.parseInt(num);
        } catch (Exception e) {
            return 100;
        }
    }

    private float parseFloat(String num) {
        try {
            return Float.parseFloat(num);
        } catch (Exception e) {
            return 0.0f;
        }
    }*/

    public void next(){
        showMessage("next");
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            infoMessage();
        }else {
            mSpeech.flush();
            onSaid();
        }
    }

    public void stop(){
        showMessage("stop");
        if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
        mSpeech.flush();
        onSaid();
    }

    @Override
    public void onSaid() {
        setVolumeToOldValue();
        stopSelf();
        if (mListener!=null) mListener.onServiceStop();
    }

    private void setVolumeToOldValue(){
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_SHOW_UI);
    }

    MediaPlayer.OnCompletionListener onStartTrackCompletionListener = new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mMediaPlayer) {
            mMediaPlayer.release();
            showMessage("intro ended");
            infoMessage();

        }
    };

    private class PlayIntroTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            playIntro();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    /*private class GetWeatherTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... urls) {
            try {
                return getOneWeather(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            parseWeatherJSON(result);
            super.onPostExecute(result);
        }
    }*/

    IWeatherListener weatherListener = new IWeatherListener() {
        @Override
        public void putWeather(WeatherJson weatherJson) {
            CurrentCondition currentCondition = weatherJson.getData().getCurrentCondition().get(0);
            int temp = currentCondition.getTempC();
            int temp_feels = currentCondition.getFeelsLikeC();
            double precip = currentCondition.getPrecipMM();

            List<Hourly> hourlies = weatherJson.getData().getWeather().get(0).getHourly();
            int i = 0;
            int tempD = 100, tempE = 100;
            double precipD = 0.0, precipE = 0.0;
            for (Hourly hourly : hourlies) {
                String time = hourly.getTime();
                if (time.equals("1300")) {
                    tempD = hourly.getTempC();
                    precipD = hourly.getPrecipMM();
                }
                if (time.equals("1900")) {
                    tempE = hourly.getTempC();
                    precipE = hourly.getPrecipMM();
                }
            }
            mSpeech.say("За окном " + Converter.getDegreesStr(temp) + ", "
                    + Converter.getPrecipitation(precip, temp)
                    + ", температура комфорта " + Converter.getDegreesStr(temp_feels));
            mSpeech.pause();
            mSpeech.say("Днём " + Converter.getDegreesStr(tempD) + ", "
                    + Converter.getPrecipitation(precipD, tempD));
            mSpeech.pause();
            mSpeech.sayLast("Вечером " + Converter.getDegreesStr(tempE) + ", "
                    + Converter.getPrecipitation(precipE, tempE));
        }

        @Override
        public void putError(Throwable e) {
            mSpeech.sayLast("Не удалось получить погоду.");
        }
    };

    private void showMessage(String text){
        //Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        Log.i("mylog:InfoAlarmService", text);
    }

    public interface OnServiceStopListener{
        public void onServiceStop();
    }
}
