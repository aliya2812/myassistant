package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hourly {

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("tempC")
    @Expose
    private Integer tempC;

    @SerializedName("tempF")
    @Expose
    private Integer tempF;

    @SerializedName("windspeedMiles")
    @Expose
    private Integer windSpeedMiles;

    @SerializedName("windspeedKmph")
    @Expose
    private Integer windSpeedKmph;

    @SerializedName("winddirDegree")
    @Expose
    private Integer windDirDegree;

    @SerializedName("winddir16Point")
    @Expose
    private String windDir16Point;

    @SerializedName("weatherCode")
    @Expose
    private Integer weatherCode;

    /*@SerializedName("weatherIconUrl")
    @Expose
    private List<String> weatherIconUrl;

    @SerializedName("weatherDesc")
    @Expose
    private List<String> weatherDesc;*/

    @SerializedName("precipMM")
    @Expose
    private Double precipMM;

    @SerializedName("humidity")
    @Expose
    private Integer humidity;

    @SerializedName("visibility")
    @Expose
    private Integer visibility;

    @SerializedName("pressure")
    @Expose
    private Integer pressure;

    @SerializedName("cloudcover")
    @Expose
    private Integer cloudCover;

    @SerializedName("HeatIndexC")
    @Expose
    private Integer heatIndexC;

    @SerializedName("HeatIndexF")
    @Expose
    private Integer heatIndexF;

    @SerializedName("DewPointC")
    @Expose
    private Integer dewPointC;

    @SerializedName("DewPointF")
    @Expose
    private Integer dewPointF;

    @SerializedName("WindChillC")
    @Expose
    private Integer windChillC;

    @SerializedName("WindChillF")
    @Expose
    private Integer windChillF;

    @SerializedName("WindGustMiles")
    @Expose
    private Integer windGustMiles;

    @SerializedName("WindGustKmph")
    @Expose
    private Integer windGustKmph;

    @SerializedName("FeelsLikeC")
    @Expose
    private Integer feelsLikeC;

    @SerializedName("FeelsLikeF")
    @Expose
    private Integer feelsLikeF;

    @SerializedName("chanceofrain")
    @Expose
    private Integer chanceOfRain;

    @SerializedName("chanceofremdry")
    @Expose
    private Integer chanceOfRemdry;

    @SerializedName("chanceofwindy")
    @Expose
    private Integer chanceOfWindy;

    @SerializedName("chanceofovercast")
    @Expose
    private Integer chanceOfOvercast;

    @SerializedName("chanceofsunshine")
    @Expose
    private Integer chanceOfSunshine;

    @SerializedName("chanceoffrost")
    @Expose
    private Integer chanceOfFrost;

    @SerializedName("chanceofhightemp")
    @Expose
    private Integer chanceOfHightemp;

    @SerializedName("chanceoffog")
    @Expose
    private Integer chanceOfFog;

    @SerializedName("chanceofsnow")
    @Expose
    private Integer chanceOfSnow;

    @SerializedName("chanceofthunder")
    @Expose
    private Integer chanceOfThunder;

    public String getTime() {
        return time;
    }

    public Integer getTempC() {
        return tempC;
    }

    public Integer getTempF() {
        return tempF;
    }

    public Integer getWindSpeedMiles() {
        return windSpeedMiles;
    }

    public Integer getWindSpeedKmph() {
        return windSpeedKmph;
    }

    public Integer getWindDirDegree() {
        return windDirDegree;
    }

    public String getWindDir16Point() {
        return windDir16Point;
    }

    public Integer getWeatherCode() {
        return weatherCode;
    }

    /*public List<String> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public List<String> getWeatherDesc() {
        return weatherDesc;
    }*/

    public Double getPrecipMM() {
        return precipMM;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public Integer getPressure() {
        return pressure;
    }

    public Integer getCloudCover() {
        return cloudCover;
    }

    public Integer getHeatIndexC() {
        return heatIndexC;
    }

    public Integer getHeatIndexF() {
        return heatIndexF;
    }

    public Integer getDewPointC() {
        return dewPointC;
    }

    public Integer getDewPointF() {
        return dewPointF;
    }

    public Integer getWindChillC() {
        return windChillC;
    }

    public Integer getWindChillF() {
        return windChillF;
    }

    public Integer getWindGustMiles() {
        return windGustMiles;
    }

    public Integer getWindGustKmph() {
        return windGustKmph;
    }

    public Integer getFeelsLikeC() {
        return feelsLikeC;
    }

    public Integer getFeelsLikeF() {
        return feelsLikeF;
    }

    public Integer getChanceOfRain() {
        return chanceOfRain;
    }

    public Integer getChanceOfRemdry() {
        return chanceOfRemdry;
    }

    public Integer getChanceOfWindy() {
        return chanceOfWindy;
    }

    public Integer getChanceOfOvercast() {
        return chanceOfOvercast;
    }

    public Integer getChanceOfSunshine() {
        return chanceOfSunshine;
    }

    public Integer getChanceOfFrost() {
        return chanceOfFrost;
    }

    public Integer getChanceOfHightemp() {
        return chanceOfHightemp;
    }

    public Integer getChanceOfFog() {
        return chanceOfFog;
    }

    public Integer getChanceOfSnow() {
        return chanceOfSnow;
    }

    public Integer getChanceOfThunder() {
        return chanceOfThunder;
    }
}
