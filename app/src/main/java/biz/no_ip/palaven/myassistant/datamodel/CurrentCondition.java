package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentCondition {

    @SerializedName("observation_time")
    @Expose
    private String observationTime;

    @SerializedName("temp_C")
    @Expose
    private Integer tempC;

    @SerializedName("temp_F")
    @Expose
    private Integer tempF;

    @SerializedName("weatherCode")
    @Expose
    private Integer weatherCode;

    /*@SerializedName("weatherIconUrl")
    @Expose
    private List<String> weatherIconUrl;

    @SerializedName("weatherDesc")
    @Expose
    private List<String> weatherDesc;*/

    @SerializedName("windspeedMiles")
    @Expose
    private Integer windSpeedMiles;

    @SerializedName("windspeedKmph")
    @Expose
    private Integer windSpeedKmph;

    @SerializedName("winddirDegree")
    @Expose
    private Integer windDirDegree;

    @SerializedName("winddir16Point")
    @Expose
    private String windDir16Point;

    @SerializedName("precipMM")
    @Expose
    private Double precipMM;

    @SerializedName("humidity")
    @Expose
    private Integer humidity;

    @SerializedName("visibility")
    @Expose
    private Integer visibility;

    @SerializedName("pressure")
    @Expose
    private Integer pressure;

    @SerializedName("cloudcover")
    @Expose
    private Integer cloudCover;

    @SerializedName("FeelsLikeC")
    @Expose
    private Integer feelsLikeC;

    @SerializedName("FeelsLikeF")
    @Expose
    private Integer feelsLikeF;

    public String getObservationTime() {
        return observationTime;
    }

    public Integer getTempC() {
        return tempC;
    }

    public Integer getTempF() {
        return tempF;
    }

    public Integer getWeatherCode() {
        return weatherCode;
    }

    /*public List<String> getWeatherIconUrl() {
        return weatherIconUrl;
    }

    public List<String> getWeatherDesc() {
        return weatherDesc;
    }*/

    public Integer getWindSpeedMiles() {
        return windSpeedMiles;
    }

    public Integer getWindSpeedKmph() {
        return windSpeedKmph;
    }

    public Integer getWindDirDegree() {
        return windDirDegree;
    }

    public String getWindDir16Point() {
        return windDir16Point;
    }

    public Double getPrecipMM() {
        return precipMM;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public Integer getPressure() {
        return pressure;
    }

    public Integer getCloudCover() {
        return cloudCover;
    }

    public Integer getFeelsLikeC() {
        return feelsLikeC;
    }

    public Integer getFeelsLikeF() {
        return feelsLikeF;
    }
}
