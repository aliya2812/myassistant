package biz.no_ip.palaven.myassistant;

import biz.no_ip.palaven.myassistant.network.ApiServices;
import biz.no_ip.palaven.myassistant.network.RestInterface;

public class BaseModel {

    protected ApiServices service;

    public BaseModel() {
        service = RestInterface.getService();
    }
}
