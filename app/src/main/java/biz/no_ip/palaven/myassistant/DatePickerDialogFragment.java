package biz.no_ip.palaven.myassistant;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;


public class DatePickerDialogFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

    //public static final String ARG_DATE = "date";

    private DatePickedListener mListener;
    private Calendar mDate;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //mDate = (Calendar) getArguments().getSerializable(ARG_DATE);
        //if (mDate == null) {
            mDate = Calendar.getInstance();
        //}

        // установим текущую дату в диалоговом окне
        int year = mDate.get(Calendar.YEAR);
        int month = mDate.get(Calendar.MONTH);
        int day = mDate.get(Calendar.DAY_OF_MONTH);

        // создадим экземпляр класса DatePickerDialog и вернем его
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onAttach(Activity activity) {
        // when the fragment is initially shown (i.e. attached to the activity),
        // cast the activity to the callback interface type
        super.onAttach(activity);
        try {
            mListener = (DatePickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + DatePickedListener.class.getName());
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Пользователь выбрал дату. Выводим его в текстовой метке
        mDate.set(Calendar.YEAR, year);
        mDate.set(Calendar.MONTH, month);
        mDate.set(Calendar.DAY_OF_MONTH, day);

        mListener.onDatePicked(mDate);
    }

    public interface DatePickedListener {
        void onDatePicked(Calendar time);
    }
}
