package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherRequest {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("query")
    @Expose
    private String query;
}
