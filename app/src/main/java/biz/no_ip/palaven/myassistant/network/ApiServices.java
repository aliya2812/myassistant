package biz.no_ip.palaven.myassistant.network;

import biz.no_ip.palaven.myassistant.datamodel.WeatherJson;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiServices {

    @GET("free/v2/weather.ashx?key=1abaf4a17e2a72f8f1c17ae13c369")
    Observable<WeatherJson> getWeather(@Query("q") String coordinates,
                                       @Query("num_of_days") int numOfDays,
                                       @Query("tp") int timePeriod,
                                       @Query("format") String format);
}
