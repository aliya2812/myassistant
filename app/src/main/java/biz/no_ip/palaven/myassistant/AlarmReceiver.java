package biz.no_ip.palaven.myassistant;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        /*Intent serviceIntent = new Intent(context, InfoAlarmService.class);
        context.startService(serviceIntent);
        showMessage("service started");*/
        Intent activityIntent = new Intent(context, MainActivity.class);
        activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(activityIntent);
        showMessage("activity started");

        Intent myIntent = new Intent(context, MainActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, myIntent, 0);
        /*PendingIntent nextIntent = PendingIntent.getService(context, InfoAlarmService.REQUEST_NEXT,
                serviceIntent, 0);
        PendingIntent stopIntent = PendingIntent.getService(context, InfoAlarmService.REQUEST_STOP,
                serviceIntent, 0);
        Notification.Action nextAction = null;
        Notification.Action stopAction = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

             nextAction = new Notification.Action.Builder(
                    Icon.createWithResource(context, R.drawable.ic_next),
                    "Next",
                    nextIntent)
                    .build();
             stopAction = new Notification.Action.Builder(
                    Icon.createWithResource(context, R.drawable.ic_stop),
                    "Stop",
                    stopIntent)
                    .build();
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH){
            nextAction = new Notification.Action.Builder(
                    R.drawable.ic_next,
                    "Next",
                    nextIntent)
                    .build();
            stopAction = new Notification.Action.Builder(
                    R.drawable.ic_stop,
                    "Stop",
                    stopIntent)
                    .build();
        }*//*else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            nextAction = new Notification.Action(
                    R.drawable.ic_next,
                    "Next",
                    nextIntent);
            stopAction = new Notification.Action(
                    R.drawable.ic_stop,
                    "Stop",
                    stopIntent);
        }*/

        NotificationManager nm = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setTicker("Будильник")
                .setContentTitle("Пора вставать")
                .setContentText("Текущее время" + " " +
                        DateFormat.format("HH:mm", System.currentTimeMillis()).toString())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setWhen(System.currentTimeMillis());
        /*if(nextAction != null) builder.addAction(nextAction);
        if(stopAction != null) builder.addAction(stopAction);*/
        Notification notification;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        } else {
            notification = builder.getNotification();
        }
        nm.notify(1, notification);
    }

    private void showMessage(String text){
        //Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
        Log.i("mylog:AlarmReceiver", text);
    }
}
