package biz.no_ip.palaven.myassistant.weather;

import biz.no_ip.palaven.myassistant.BaseModel;
import biz.no_ip.palaven.myassistant.datamodel.WeatherJson;
import rx.Observable;

/**
 * Created by aliga on 11.09.2016.
 */

public class WeatherModel extends BaseModel{

    public Observable<WeatherJson> loadWeather(double latitude, double longitude, int numOfDays, int timePeriod) {
        String coordinates = Double.toString(latitude).concat(",").concat(Double.toString(longitude));
        return service.getWeather(coordinates, numOfDays, timePeriod, "json");
    }
}
