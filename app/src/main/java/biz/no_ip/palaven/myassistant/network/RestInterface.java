package biz.no_ip.palaven.myassistant.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestInterface {

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://api.worldweatheronline.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(new OkHttpClient())
            .build();

    public static ApiServices getService() {
        return retrofit.create(ApiServices.class);
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }
}
