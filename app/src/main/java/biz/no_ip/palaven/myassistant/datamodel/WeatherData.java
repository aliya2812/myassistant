package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherData {

    @SerializedName("request")
    @Expose
    private List<WeatherRequest> request;

    @SerializedName("current_condition")
    @Expose
    private List<CurrentCondition> currentCondition;

    @SerializedName("weather")
    @Expose
    private List<Weather> weather;

    public List<CurrentCondition> getCurrentCondition() {
        return currentCondition;
    }

    public List<Weather> getWeather() {
        return weather;
    }
}
