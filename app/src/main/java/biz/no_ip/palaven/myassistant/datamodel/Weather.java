package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Weather {

    @SerializedName("date")
    @Expose
    private Date date;

    @SerializedName("astronomy")
    @Expose
    private List<Astronomy> astronomy;

    @SerializedName("maxtempC")
    @Expose
    private Integer maxTempC;

    @SerializedName("maxtempF")
    @Expose
    private Integer maxTempF;

    @SerializedName("mintempC")
    @Expose
    private Integer minTempC;

    @SerializedName("mintempF")
    @Expose
    private Integer minTempF;

    @SerializedName("uvIndex")
    @Expose
    private Integer uvIndex;

    @SerializedName("hourly")
    @Expose
    private List<Hourly> hourly;

    public Date getDate() {
        return date;
    }

    public List<Astronomy> getAstronomy() {
        return astronomy;
    }

    public Integer getMaxTempC() {
        return maxTempC;
    }

    public Integer getMaxTempF() {
        return maxTempF;
    }

    public Integer getMinTempC() {
        return minTempC;
    }

    public Integer getMinTempF() {
        return minTempF;
    }

    public Integer getUvIndex() {
        return uvIndex;
    }

    public List<Hourly> getHourly() {
        return hourly;
    }
}
