package biz.no_ip.palaven.myassistant.weather;

import biz.no_ip.palaven.myassistant.datamodel.WeatherJson;

public interface IWeatherListener {

    void putWeather(WeatherJson weatherJson);

    void putError(Throwable e);
}
