package biz.no_ip.palaven.myassistant;

import android.text.format.DateFormat;

import java.util.Calendar;

public class Converter {

    public static String getHoursStr(int hours) {
        if ((hours < 0) || (hours > 23)) return "";
        int lastDigit = hours % 10;
        StringBuilder str = new StringBuilder();
        String numStr = numToText(hours, false);
        if (numStr.equals("ошибка")) return "";
        str.append(numStr);
        if ((hours > 0) && (hours < 5) || (hours > 20)) {
            if (lastDigit == 1) {
                str.append("час ");
            } else {
                str.append("часа ");
            }
        } else {
            str.append("часов ");
        }
        return str.toString();
    }

    public static String getMinutesStr(int minutes) {
        if ((minutes < 0) || (minutes > 59)) return "";
        int lastDigit = minutes % 10;
        StringBuilder str = new StringBuilder();
        String numStr = numToText(minutes, true);
        if (numStr.equals("ошибка")) return "";
        str.append(numStr);
        if ((minutes < 5) || (minutes > 20)) {
            if (lastDigit == 1) {
                str.append("минута ");
            } else if ((lastDigit > 1) && (lastDigit < 5)) {
                str.append("минуты ");
            } else {
                str.append("минут ");
            }
        } else {
            str.append("минут ");
        }
        return str.toString();
    }

    public static String getDateStr(Calendar date) {
        StringBuilder str = new StringBuilder();
        String dateStr = dateToText(date.get(Calendar.DATE));
        if (dateStr.equals("ошибка")) return "";
        str.append(dateStr);
        str.append(DateFormat.format("MMMM", date.getTimeInMillis()).toString());
        str.append(" ");
        return str.toString();
    }

    public static String getDegreesStr(int degrees) {
        if ((degrees < -39) || (degrees > 39)) return "";
        StringBuilder str = new StringBuilder();
        if (degrees < 0) {
            str.append("минус ");
        } else if (degrees > 0) {
            str.append("плюс ");
        }
        int tmpDegrees = degrees > 0 ? degrees : -1 * degrees;
        int lastDigit = tmpDegrees % 10;
        String numStr = numToText(tmpDegrees, false);
        if (numStr.equals("ошибка")) return "";
        str.append(numStr);
        if ((tmpDegrees < 5) || (tmpDegrees > 20)) {
            if (lastDigit == 1) {
                str.append("градус ");
            } else if ((lastDigit > 1) && (lastDigit < 5)) {
                str.append("градуса ");
            } else {
                str.append("градусов ");
            }
        } else {
            str.append("градусов ");
        }
        return str.toString();
    }

    public static String numToText(int num, boolean isFeminine) {
        if ((num < 0) || (num > 99)) return "ошибка";
        StringBuilder result = new StringBuilder();
        int firstDigit = num / 10;
        int lastDigit = num % 10;
        switch (firstDigit) {
            case 2:
                result.append("двадцать ");
                break;
            case 3:
                result.append("тридцать ");
                break;
            case 4:
                result.append("сорок ");
                break;
            case 5:
                result.append("пятьдесят ");
                break;
            case 6:
                result.append("шестьдесят ");
                break;
            case 7:
                result.append("семьдесят ");
                break;
            case 8:
                result.append("восемьдесят ");
                break;
            case 9:
                result.append("девяносто ");
                break;
        }
        if (firstDigit == 1) {
            switch (lastDigit) {
                case 0:
                    result.append("десять ");
                    break;
                case 1:
                    result.append("одиннадцать ");
                    break;
                case 2:
                    result.append("двенадцать ");
                    break;
                case 3:
                    result.append("тринадцать ");
                    break;
                case 4:
                    result.append("четырнадцать ");
                    break;
                case 5:
                    result.append("пятнадцать ");
                    break;
                case 6:
                    result.append("шестнадцать ");
                    break;
                case 7:
                    result.append("семнадцать ");
                    break;
                case 8:
                    result.append("восемнадцать ");
                    break;
                case 9:
                    result.append("девятнадцать ");
                    break;
            }
        } else {
            switch (lastDigit) {
                case 0:
                    if (firstDigit < 1) {
                        result.append("ноль ");
                    }
                    break;
                case 1:
                    if (isFeminine) {
                        result.append("одна ");
                    } else {
                        result.append("один ");
                    }
                    break;
                case 2:
                    if (isFeminine) {
                        result.append("две ");
                    } else {
                        result.append("два ");
                    }
                    break;
                case 3:
                    result.append("три ");
                    break;
                case 4:
                    result.append("четыре ");
                    break;
                case 5:
                    result.append("пять ");
                    break;
                case 6:
                    result.append("шесть ");
                    break;
                case 7:
                    result.append("семь ");
                    break;
                case 8:
                    result.append("восемь ");
                    break;
                case 9:
                    result.append("девять ");
                    break;
            }
        }
        return result.toString();
    }

    public static String dateToText(int num) {
        if ((num < 1) || (num > 31)) return "ошибка";
        StringBuilder result = new StringBuilder();
        int lastDigit = num % 10;
        if ((num > 9) && (num < 21)) {
            switch (num) {
                case 10:
                    result.append("десятое ");
                    break;
                case 11:
                    result.append("одиннадцатое ");
                    break;
                case 12:
                    result.append("двенадцатое ");
                    break;
                case 13:
                    result.append("тринадцатое ");
                    break;
                case 14:
                    result.append("четырнадцатое ");
                    break;
                case 15:
                    result.append("пятнадцатое ");
                    break;
                case 16:
                    result.append("шестнадцатое ");
                    break;
                case 17:
                    result.append("семнадцатое ");
                    break;
                case 18:
                    result.append("восемнадцатое ");
                    break;
                case 19:
                    result.append("девятнадцатое ");
                    break;
                case 20:
                    result.append("двадцатое ");
                    break;
            }
        } else if (num == 30) {
            result.append("тридцатое ");
        } else {
            if (num > 30) {
                result.append("тридцать ");
            } else if (num > 20) {
                result.append("двадцать ");
            }
            switch (lastDigit) {
                case 1:
                    result.append("первое ");
                    break;
                case 2:
                    result.append("второе ");
                    break;
                case 3:
                    result.append("третье ");
                    break;
                case 4:
                    result.append("четвертое ");
                    break;
                case 5:
                    result.append("пятое ");
                    break;
                case 6:
                    result.append("шестое ");
                    break;
                case 7:
                    result.append("седьмое ");
                    break;
                case 8:
                    result.append("восьмое ");
                    break;
                case 9:
                    result.append("девятое ");
                    break;
            }
        }

        return result.toString();

    }

    public static String getPrecipitation(float precip, int temp) {
        if (precip <= 0) return "без осадков";
        if (temp > 0) {
            return "дождь";
        } else {
            return "снег";
        }
    }

    public static String getPrecipitation(double precip, int temp) {
        if (precip <= 0) return "без осадков";
        if (temp > 0) {
            return "дождь";
        } else {
            return "снег";
        }
    }

    public static String getWeekDay(int dayOfWeek) {
        switch (dayOfWeek) {
            case 2:
                return "понедельник";
            case 3:
                return "вторник";
            case 4:
                return "среда";
            case 5:
                return "четверг";
            case 6:
                return "пятница";
            case 7:
                return "суббота";
            case 1:
                return "воскресенье";
            default:
                return "";
        }
    }
}
