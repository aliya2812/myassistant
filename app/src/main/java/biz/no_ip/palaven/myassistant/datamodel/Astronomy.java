package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Astronomy {

    @SerializedName("sunrise")
    @Expose
    private String sunrise;

    @SerializedName("sunset")
    @Expose
    private String sunset;

    @SerializedName("moonrise")
    @Expose
    private String moonrise;

    @SerializedName("moonset")
    @Expose
    private String moonset;
}
