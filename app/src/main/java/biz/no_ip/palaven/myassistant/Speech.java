package biz.no_ip.palaven.myassistant;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

public class Speech {

    TextToSpeech mTts;
    AudioManager audioManager;
    HashMap<String, String> ttsParameters;
    OnSaidListener mListener;

    public Speech(Context context) {
        mListener = (OnSaidListener)context;
        audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        mTts = new TextToSpeech(context, onInitListener);
        ttsParameters = new HashMap<>();
        ttsParameters.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "stringId");
        //final int currentPlay=audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mTts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onDone(String utteranceId) {
                if (utteranceId.equals("last")){
                    showMessage("last message said");
                    //setVolume(currentPlay);
                    mListener.onSaid();
                }
            }

            @Override
            public void onError(String utteranceId) {
                showMessage("message error");
                //setVolume(currentPlay);
                mListener.onSaid();
            }

            @Override
            public void onStart(String utteranceId) {
                if (utteranceId.equals("first")) {
                    showMessage("first message start");
                    //setVolume(13);
                }
            }
        });
    }

    /*private void setVolume(int volume){
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, AudioManager.FLAG_SHOW_UI);
    }*/

    TextToSpeech.OnInitListener onInitListener = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {
                mTts.setLanguage(new Locale("ru"));
            }
        }
    };

    public void say(String text, String id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTts.speak(text, TextToSpeech.QUEUE_ADD, null, id);
        } else {
            mTts.speak(text, TextToSpeech.QUEUE_ADD, ttsParameters);
        }
    }

    public void say(String text){
        say(text, "stringId");
    }

    public void sayFirst(String text){
        say(text, "first");
    }

    public void sayLast(String text){
        say(text, "last");
    }

    public void pause() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTts.playSilentUtterance(400, TextToSpeech.QUEUE_ADD, null);
        } else {
            mTts.playSilence(400, TextToSpeech.QUEUE_ADD, null);
        }
    }

    public void flush() {
        showMessage("flush");
        mTts.stop();
    }

    public void destroy() {
        showMessage("onDestroy");
        mTts.stop();
        mTts.shutdown();
    }

    private void showMessage(String text){
        //Toast.makeText((InfoAlarmService)mListener, text, Toast.LENGTH_SHORT).show();
        Log.i("mylog:Speech", text);
    }

    public interface OnSaidListener{
        public void onSaid();
    }
}
