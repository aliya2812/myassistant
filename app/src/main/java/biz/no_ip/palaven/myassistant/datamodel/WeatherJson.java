package biz.no_ip.palaven.myassistant.datamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherJson {

    @SerializedName("data")
    @Expose
    private WeatherData data;

    public WeatherData getData() {
        return data;
    }
}
