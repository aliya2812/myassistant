package biz.no_ip.palaven.myassistant;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity
        implements DatePickerDialogFragment.DatePickedListener,
                    TimePickerDialogFragment.TimePickedListener,
                    InfoAlarmService.OnServiceStopListener{

    TextView textView;
    TextView textView2;
    InfoAlarmService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textView = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        showMessage("onCreate");

        Intent intent = getIntent();
        if (intent.getAction()==null){
            startAlarm();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id==R.id.action_alarm) {
            //choose date
            DialogFragment dateFragment = new DatePickerDialogFragment();
            dateFragment.show(getSupportFragmentManager(), "datePicker");
        }else if(id==R.id.action_stop_alarm) {
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                    0, intent, 0);
            alarmManager.cancel(pendingIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        //showMessage("onDestroy");
        if (mService!=null) {
            showMessage("unbind on destroy");
            unbindService(mConnection);
        }
        super.onDestroy();
    }

    public void onClick(View view) {
        startAlarm();
    }

    private void startAlarm(){
        Intent intent = new Intent(MainActivity.this, InfoAlarmService.class);
        //startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            InfoAlarmService.LocalBinder binder = (InfoAlarmService.LocalBinder) service;
            mService = binder.getService();
            mService.setListener(MainActivity.this);
            showMessage("service bound");
            mService.startAlarm();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            showMessage("service disconnected");
        }
    };

    public void onNext(View view) {
        if (mService!=null) {
            showMessage("service next");
            mService.next();
        }
    }

    public void onStop(View view) {
        if (mService!=null) {
            showMessage("service stop");
            mService.stop();
            //unbindService(mConnection);
        }
    }

    @Override
    public void onDatePicked(Calendar time) {
        DialogFragment timeFragment = new TimePickerDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(TimePickerDialogFragment.ARG_TIME, time);
        timeFragment.setArguments(args);
        timeFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void onTimePicked(Calendar time) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),
                0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), 24*60*60*1000, pendingIntent);
    }

    @Override
    public void onServiceStop() {
        if (mService!=null) {
            showMessage("service stopped self");
            unbindService(mConnection);
            mService = null;
        }
    }

    private void showMessage(String text){
        //Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
        Log.i("mylog:MainActivity", text);
    }
}
